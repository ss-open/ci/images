#!/usr/bin/env bash
set -e
# Ensures hidden files to be included in globbing expressions.
shopt -s dotglob

status=0
script_dir=$(dirname "$0")

fail() {
	local message="$1"
	echo "[fail] ${message}" >&2
	status=1
}

src_path="${script_dir}"/src
for image_path in "${src_path}"/*; do
	if ! test -d "${image_path}"; then
		fail "${image_path}: must be a directory."
		continue
	fi
	dockerfile_path="${image_path}/Dockerfile"
	test -f "${dockerfile_path}" || fail "${dockerfile_path}: missing or not a file."
	readme_path="${image_path}/README.md"
	test -f "${readme_path}" || fail "${readme_path}: missing or not a file."
	ci_path="${image_path}/.gitlab-ci.yml"
	test -f "${ci_path}" || fail "${ci_path}: missing or not a file."
done

exit $status
