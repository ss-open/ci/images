# php-cs-fixer

- Vendor: <https://cs.symfony.com/>
- Documentation: <https://cs.symfony.com/doc/usage.html>

## Usage

```sh
docker run -w$(pwd) -v$(pwd):$(pwd) registry.gitlab.com/ss-open/ci/images/php-cs-fixer
```
