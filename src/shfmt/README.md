# shfmt

Vendor: <https://github.com/mvdan/sh>

## Usage

```sh
docker run -w$(pwd) -v$(pwd):$(pwd) registry.gitlab.com/ss-open/ci/images/shfmt shfmt -w .
```

Note: Replace `-w` by `-d` on CI job integration.
