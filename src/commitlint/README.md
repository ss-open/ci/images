# commitlint

- Vendor: <https://commitlint.js.org>
- Documentation: <https://commitlint.js.org/#/?id=getting-started>

## Usage

```sh
docker run -w$(pwd) -v$(pwd):$(pwd) registry.gitlab.com/ss-open/ci/images/commitlint --form=origin/main
```
