# shellcheck

- Vendor: <https://www.shellcheck.net>
- Documentation: <https://www.shellcheck.net/wiki/Home>

## Usage

```sh
docker run -w$(pwd) -v$(pwd):$(pwd) registry.gitlab.com/ss-open/ci/images/shellcheck find . -name \*.sh -type f | sort | xargs shellcheck -x
```
