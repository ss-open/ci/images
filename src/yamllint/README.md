# yamllint

Vendor: <https://github.com/adrienverge/yamllint>

## Usage

```sh
docker run -w$(pwd) -v$(pwd):$(pwd) registry.gitlab.com/ss-open/ci/images/yamllint .
```

## Project level configuration

To override the configuration at project level, create a `.yamllint` referencing the main file:

```yaml
extends: /.yamllint
```

Then, configure the ignore rules or anything else you need. For example:

```yaml
extends: /.yamllint

ignore: |
  node_modules
  vendor
```
